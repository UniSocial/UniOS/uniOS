/*
https://help.ubuntu.com/community/SSHFS
https://www.digitalocean.com/community/tutorials/how-to-use-sshfs-to-mount-remote-file-systems-over-ssh
*/

project='example'
in=~/dev/$project
node='me@192.168.77.114'

fsProject(){
  mkdir -p $in
  sshfs -C -o cache=yes $node:./$project $in
}


unmount(){
  fusermount -u $dir
}
